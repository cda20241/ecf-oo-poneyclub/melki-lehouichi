package fr.ecfafpa.petitponey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Ecurie {
    private String nom;
    private ArrayList<Salarie> salaries=new ArrayList<Salarie>();
    private ArrayList<Poney> poneys= new ArrayList<Poney>();
    private ArrayList<Box> box=new ArrayList<Box>();

    //CONSTRUCTEURS
    public Ecurie(String nom) {
        this.nom = nom;
    }

    //METHODES

    /**
     * cette fonction crée et ajoute un box à l'écurie et returne le box créé
     * @param nom
     * @param taille
     * @param quantiteFoinCourante
     * @param quantiteFoinMax
     * @return
     */
    public Box ajouterBox(String nom, Integer taille, Integer quantiteFoinCourante, Integer quantiteFoinMax){
        Box box = new Box(nom,taille,quantiteFoinCourante,quantiteFoinMax);
        this.box.add(box);
        return box;
    }

    /**
     * cette fonction crée et ajoute un poney à l'écurie
     * @param nom
     * @param poids
     * @param texture
     * @param longueur
     * @return
     */
    public Poney aquerirPoney(String nom, Integer poids, Enum.TEXTURE texture, Integer longueur){
        Poney poney = new Poney(nom,poids,texture,longueur);
        this.poneys.add(poney);
        return poney;
    }

    /**
     * cette fonction pernd en paramètre un poney et rendre null le box dans poney
     * @param poney
     */
    public void envoyerPaitre(Poney poney){
        for (Box box : this.box){
            for(Poney unPoney : box.getPoney()){
                if (unPoney==poney){
                    box.getPoney().remove(unPoney);
                    poney.setBox(null);
                }
            }
        }
    }

    /**
     * cette fonction prend en paramètre un poney et un box. Elle met le poney dans et ajoute le box au poney
     * @param poney
     * @param box
     */
    public void rentrerPoney(Poney poney, Box box){
        if (box.getPoney().size()<box.getTaille()){
            if (box.getPoney().contains(poney)){
                System.out.println("Le poney est déjà dans ce box, on ne peut pas le rentrer !");
            }else {
                box.ajouterPoney(poney);
                poney.setBox(box);
            }
        }
    }

    /**
     * cette fonction ajoute un salarie à l'entreprise et une entreprise au salarie
     * @param salarie
     */
    public void recruterSalarie(Salarie salarie){
        salarie.setEntreprise(this);
        this.salaries.add(salarie);
    }

    /**
     * cette fonction retire le salrie de l'entreprise et rendre null l'entreprise dans salrié
     * @param salarie
     */
    public void virerSalarie(Salarie salarie){
        this.salaries.remove(salarie);
        salarie.setEntreprise(null);
    }

    /**
     * cette fonction affiche le taux de remplissage de chaque box dans l'écurie
     */
    public void afficherRemplissageDesBox(){
        for (Box box : this.box){
            System.out.println(box.getNom()+" : "+box.getPourcentage() +" %");
        }
    }

    /**
     * cette fonction prend en paramètre une quantité de foin, répartit le foin de façon équitable dans les
     * box et retourne un HashMap avec les nouvelles quantité
     * @param quantiteFoin
     * @return
     */
    public Map<String, Integer> repartirFoin(Integer quantiteFoin){
        Map<String,Integer> hashMapRepartition = new HashMap<>();
        Integer sommeQuantiteCourante=0;
        Integer sommeQuantiteMaximale=0;
        //On parcourt les box pour additionner les quantités courante et maximale et rendre les sommes
        for (Box box : this.box){
            sommeQuantiteCourante+=box.getQuantiteFoinCourante();
            sommeQuantiteMaximale+=box.getQuantiteFoinMax();
        }
        //On calcule le quotient pour la répartition
        float quotient=(float)(sommeQuantiteCourante+quantiteFoin) / sommeQuantiteMaximale;
        //On parcourt les box pour répartir les quantité et implémenter le HashMap
        for(Box box : this.box){
            int seuil= (int) ((int) box.getQuantiteFoinMax()*quotient);
            box.setQuantiteFoinCourante(seuil);
            hashMapRepartition.put(box.getNom(), box.getQuantiteFoinCourante());
        }
        return hashMapRepartition;
    }



    // GETTER & SETTER
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ArrayList<Salarie> getSalaries() {
        return salaries;
    }

    public void setSalaries(ArrayList<Salarie> salaries) {
        this.salaries = salaries;
    }

    public ArrayList<Poney> getPoneys() {
        return poneys;
    }

    public void setPoneys(ArrayList<Poney> poneys) {
        this.poneys = poneys;
    }

    public ArrayList<Box> getBox() {
        return box;
    }

    public void setBox(ArrayList<Box> box) {
        this.box = box;
    }
}
