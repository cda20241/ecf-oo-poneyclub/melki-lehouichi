package fr.ecfafpa.petitponey;

import java.util.ArrayList;
import java.util.List;

public class Salarie extends Personne {
    private String fonction;
    private Ecurie entreprise;

    private List<Livraison> livraisons= new ArrayList<>();

    // CONSTRUCTEUR
    public Salarie(String nom, String prenom, String fonction) {
        super(nom, prenom);
        this.fonction = fonction;
    }

    // METHODES
    public void receptionnerLivraison(Livraison livraison){
        this.livraisons.add(livraison);
    }

    // GETTER & SETTER

    public String getFonction() {
        return fonction;
    }

    public Ecurie getEntreprise() {
        return entreprise;
    }

    public List<Livraison> getLivraisons() {
        return livraisons;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public void setEntreprise(Ecurie entreprise) {
        this.entreprise = entreprise;
    }

    public void setLivraisons(List<Livraison> livraisons) {
        this.livraisons = livraisons;
    }
}
