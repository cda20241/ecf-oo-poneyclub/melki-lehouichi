package fr.ecfafpa.petitponey;
import fr.ecfafpa.petitponey.Enum.TYPEFOIN;

public class Criniere {
    private Integer longueur;
    private Enum.TEXTURE texture;

    // CONSTRUCTEUR
    public Criniere( Enum.TEXTURE texture, Integer longueur) {
        this.longueur = longueur;
        this.texture = texture;
    }

    //GETTER & SETTER
    public Integer getLongueur() {
        return longueur;
    }

    public Enum.TEXTURE getTexture() {
        return texture;
    }

    public void setLongueur(Integer longueur) {
        this.longueur = longueur;
    }

    public void setTexture(Enum.TEXTURE texture) {
        this.texture = texture;
    }
}
