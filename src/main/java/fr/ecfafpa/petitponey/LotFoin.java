package fr.ecfafpa.petitponey;

public class LotFoin {
    private String numeroLot;
    private Integer quantiteFoin;
    private Enum.TYPEFOIN typeFoin;

    // CONSTRUCTEUR
    public LotFoin(String numeroLot, Integer quantiteFoin, Enum.TYPEFOIN typeFoin) {
        this.numeroLot = numeroLot;
        this.quantiteFoin = quantiteFoin;
        this.typeFoin = typeFoin;
    }

    // GETTER & SETTER
    public String getNumeroLot() {
        return numeroLot;
    }

    public Integer getQuantiteFoin() {
        return quantiteFoin;
    }

    public Enum.TYPEFOIN getTypeFoin() {
        return typeFoin;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public void setQuantiteFoin(Integer quantiteFoin) {
        this.quantiteFoin = quantiteFoin;
    }

    public void setTypeFoin(Enum.TYPEFOIN typeFoin) {
        this.typeFoin = typeFoin;
    }
}
