package fr.ecfafpa.petitponey;

public class Main {
    public static void main(String[] args) {
        // Créer des salariés
        Salarie obiwan = new Salarie("KENOBI", "Obiwan","salarié");
        Salarie luc = new Salarie("SKYWALKER", "Luc","salarié");

        //Créer l'écurie
        Ecurie ecurie = new Ecurie("Tatooine Poney Club");

        //Embaucher les salariés
        ecurie.recruterSalarie(obiwan);
        ecurie.recruterSalarie(luc);

        //Créer des box et les ajouter à l'écurie
        Box boxA= ecurie.ajouterBox("Box A",3,200,600);
        Box boxB= ecurie.ajouterBox("Box B",4,300,600);
        Box boxC= ecurie.ajouterBox("Box C",5,900,1000);
        Box boxD= ecurie.ajouterBox("Box D",6,100,800);

        // Créer et aquérir des poeys
        Poney usainBolt = ecurie.aquerirPoney("Usain Bolt",250,Enum.TEXTURE.LISSE,25);
        Poney petitTonnere = ecurie.aquerirPoney("Petit Tonnere",280,Enum.TEXTURE.FRISEE,28);

        // Rentrer les poneys dans des box
        ecurie.rentrerPoney(usainBolt,boxA);
        ecurie.rentrerPoney(petitTonnere,boxB);

        // Créer une livraison et l'attribuer à Luc pour la réception
        Livraison livraison = new Livraison(Enum.TYPEFOIN.PRAIRIE,200,"F003");
        livraison.receptionnerFoin(luc);

        // Afficher le remlissage des box
        System.out.println("Le taux de remplissage des box :");
        ecurie.afficherRemplissageDesBox();
        // Répartir la quantité du foin livré
        ecurie.repartirFoin(livraison.getLotFoin().getQuantiteFoin());
        // Réafficher le remplissage après répartition
        System.out.println("Le taux de remplissage des box après livraison et répartition :");
        ecurie.afficherRemplissageDesBox();

    }
}
