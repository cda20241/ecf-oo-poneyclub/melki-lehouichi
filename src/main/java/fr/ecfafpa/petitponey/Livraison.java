package fr.ecfafpa.petitponey;

import java.time.LocalDate;
import java.util.List;

public class Livraison {
    private LocalDate dateLivraison;
    private Salarie salarie;
    private LotFoin lotFoin;

    public Livraison(Enum.TYPEFOIN typeFoin,Integer quantiteFoin,String numeroLot) {
        this.dateLivraison = LocalDate.now();
        this.lotFoin = new LotFoin(numeroLot,quantiteFoin,typeFoin);
    }
    // METHODES

    /**
     * Cette méthode prend en paramètre une liste de livraisons et un numéro de lot et rend la date de livraison
     * si existe null sinon
     * @param livraisons
     * @param numeroLot
     * @return
     */
    public static LocalDate dateLivraisonDuLot(List<Livraison> livraisons, String numeroLot){

        for (Livraison livraison : livraisons){
            if (livraison.lotFoin.getNumeroLot()==numeroLot){
                return livraison.getDateLivraison();
            }
        }
        return null;
    }

    public void receptionnerFoin(Salarie salarie){
        // Cette méthode attribue à un salarié la réception du foin
        this.salarie=salarie;
        salarie.receptionnerLivraison(this);
    }

    // GETTER & SETTER
    public LocalDate getDateLivraison() {
        return dateLivraison;
    }

    public Salarie getSalarie() {
        return salarie;
    }

    public LotFoin getLotFoin() {
        return lotFoin;
    }

    public void setDateLivraison(LocalDate dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public void setSalarie(Salarie salarie) {
        this.salarie = salarie;
    }

    public void setLotFoin(LotFoin lotFoin) {
        this.lotFoin = lotFoin;
    }
}
