package fr.ecfafpa.petitponey;

public class Poney {
    private String nom;
    private Integer poids;
    private Criniere criniere;
    private Box box;
    private Ecurie ecurie;

    // CONSTRUCTEUR
    public Poney(String nom, Integer poids, Enum.TEXTURE texture, Integer longueur) {
        this.nom = nom;
        this.poids = poids;
        this.criniere = new Criniere(texture,longueur);
    }

    // GETTER & SETTER
    public String getNom() {
        return nom;
    }

    public Integer getPoids() {
        return poids;
    }

    public Criniere getCriniere() {
        return criniere;
    }

    public Box getBox() {
        return box;
    }

    public Ecurie getEcurie() {
        return ecurie;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    public void setCriniere(Criniere criniere) {
        this.criniere = criniere;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    public void setEcurie(Ecurie ecurie) {
        this.ecurie = ecurie;
    }
}
