package fr.ecfafpa.petitponey;

import java.util.ArrayList;
import java.util.List;

public class Box {
    private String nom;
    private Integer taille;
    private Integer quantiteFoinCourante;
    private Integer quantiteFoinMax;
    private ArrayList<Poney> poneys= new ArrayList<Poney>();

    // CONSTRUCTEURS
    public Box(String nom, Integer taille, Integer quantiteFoinCourante, Integer quantiteFoinMax) {
        this.nom=nom;
        this.taille = taille;
        this.quantiteFoinCourante = quantiteFoinCourante;
        this.quantiteFoinMax = quantiteFoinMax;
    }

    // METODHES
    public void chargerFoin(Integer quantiteFoin){
        if (this.quantiteFoinCourante + quantiteFoin <= this.quantiteFoinMax){
            this.quantiteFoinCourante+=quantiteFoin;
        } else if (this.quantiteFoinCourante + quantiteFoin > this.quantiteFoinMax  && this.quantiteFoinCourante < this.quantiteFoinMax) {
            Integer reste = (this.quantiteFoinCourante + quantiteFoin)-this.quantiteFoinMax;
            Integer ajout = quantiteFoin-reste;
            this.quantiteFoinCourante+=ajout;
            System.out.println(ajout + " kg on été chargés avec succès !");
            System.out.println(reste + " kg on été jetés !");
        } else {
            System.out.println(" Nous ne pouvons rechargé ce box car capacité maximale utilisée !");
        }
    }

    // GETTER & SETTER
    public int getPourcentage(){
        float pourcentage = (float) this.getQuantiteFoinCourante()/this.getQuantiteFoinMax()*100;
        return Math.round(pourcentage);
    }

    public void ajouterPoney(Poney poney){
        this.poneys.add(poney);
    }

    // GETTER ET SETTER

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getTaille() {
        return taille;
    }

    public void setTaille(Integer taille) {
        this.taille = taille;
    }

    public Integer getQuantiteFoinCourante() {
        return quantiteFoinCourante;
    }

    public void setQuantiteFoinCourante(Integer quantiteFoinCourante) {
        this.quantiteFoinCourante = quantiteFoinCourante;
    }

    public Integer getQuantiteFoinMax() {
        return quantiteFoinMax;
    }

    public void setQuantiteFoinMax(Integer quantiteFoinMax) {
        this.quantiteFoinMax = quantiteFoinMax;
    }

    public ArrayList<Poney> getPoney() {
        return poneys;
    }

    public void setPoney(ArrayList<Poney> poneys) {
        this.poneys = poneys;
    }
}
